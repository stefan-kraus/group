# project web-pages of Image Planet Form Discs research group

A few pointers for editing these pages:

1.  news.html contains news items of interest. Icons can be chosen from the 
range of available possibilities in css/font-awesome.min.css. An example is 
included to show how to add an image to the news item. The image should be
saved in images/.

2.  members.html contains useful information about each group member. You are 
invited to update your own section. Only a brief summary of your 
activities are recommended for inclusion here. Please provide a link to your
personal webpages if you have them. 



